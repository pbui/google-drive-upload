#!/usr/bin/env python2.7

import argparse
import httplib2
import mimetypes
import os
import sys

from apiclient.discovery    import build
from apiclient.http         import MediaFileUpload
from oauth2client           import tools
from oauth2client.client    import OAuth2WebServerFlow
from oauth2client.file      import Storage

# Configuration ----------------------------------------------------------------

CLIENT_ID     = '401870910329-1rej4cte3rcc9526lqhlmh2pbtuosj17.apps.googleusercontent.com'
CLIENT_SECRET = '3l934OvFggGcDEr5mlw1sW4X'
OAUTH_SCOPE   = 'https://www.googleapis.com/auth/drive'
REDIRECT_URI  = 'urn:ietf:wg:oauth:2.0:oob'
PARENTS       = ['0B-BwSecmKTxCfnc3X2Itb0U4VzlCZUdSOVZ3R3JNZjU2X1ZSWXowMUJvOElQeVpxQ3R5TDg']
STORAGE_PATH  = os.path.join(os.path.expanduser('~'), '.config', 'gdu', 'credentials')

# Mimetypes --------------------------------------------------------------------

MIMETYPE_BY_EXTENSION = {
    '.doc'  : 'application/msword',
    '.docx' : 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    '.pdf'  : 'application/pdf',
    '.xls'  : 'application/vnd.ms-excel',
    '.xlsx' : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
}

def determine_mimetype(path):
    mimetype = mimetypes.guess_type(file)[0]
    if mimetype is None:
        mimetype = MIMETYPE_BY_EXTENSION.get(os.path.splitext(path)[-1], 'text/plain')
    return mimetype

# Main Execution ---------------------------------------------------------------

if __name__ == '__main__':
    parser = argparse.ArgumentParser(parents=[tools.argparser])
    parser.add_argument('files', nargs='+', help='Files to upload')

    flags         = parser.parse_args()
    flow          = OAuth2WebServerFlow(CLIENT_ID, CLIENT_SECRET, OAUTH_SCOPE, redirect_uri=REDIRECT_URI)
    storage       = Storage(STORAGE_PATH)
    storage_root  = os.path.dirname(STORAGE_PATH)

    if not os.path.exists(storage_root):
        os.makedirs(storage_root)

    credentials   = storage.get()
    if credentials is None or credentials.invalid:
        credentials   = tools.run_flow(flow, storage, flags)

    http          = credentials.authorize(httplib2.Http())
    drive_service = build('drive', 'v2', http=http)

    for file in flags.files:
        media_type = determine_mimetype(file)
        media_body = MediaFileUpload(file, mimetype=media_type, resumable=True)
        body = {
            'title'         : os.path.basename(file),
            'mimeType'      : media_type,
            'parents'       : [{'id': parent} for parent in PARENTS],
        }

        response = drive_service.files().insert(body=body, media_body=media_body).execute()
        print response['alternateLink']

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
